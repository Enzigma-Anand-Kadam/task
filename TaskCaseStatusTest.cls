@isTest
public with sharing class TaskCaseStatusTest {
    @testSetup
    public static void createCase(){
        Profile objProfile = [SELECT Id FROM Profile WHERE Name = 'Standard Platform User'];
        User objUser = new User();
        objUser.Username = 'shubhssamk12345@gmail.com';
        objUser.LastName = 'K';
        objUser.Alias = 'SK';
        objUser.Email = 'shubhssamk@gmail.com';
        objUser.TimeZoneSidKey = 'Asia/Kolkata';
        objUser.LocaleSidKey = 'en_US';
        objUser.EmailEncodingKey = 'UTF-8';
        objUser.ProfileId = objProfile.Id;
        objUser.LanguageLocaleKey = 'en_US';
        insert objUser;
    }

    @isTest
    public static void testTaskCaseStatus(){
        // Create test data
        User user = [SELECT Id,Name From User Limit 1];
        Account testAccount = new Account(Name = 'Demo Account', Email__c = 'demo@example.com');
        insert testAccount;
        Contact testContact = new Contact(FirstName = 'Demo', LastName = 'Contact', Email = 'testcontact@example.com',AccountId = testAccount.Id);
        insert testContact;
        Case testCase = new Case(AccountId = testAccount.Id, OwnerId = user.Id);
        insert testCase;
        
        Test.startTest();
        TaskCaseStatus tcs = new TaskCaseStatus();
        Id batchId = Database.executeBatch(tcs);
        Test.stopTest();

        //List<Case> lstCase = [SELECT Status, (SELECT Id FROM Contacts) FROM Case];
        List<Case> lstCase = [SELECT Status FROM Case];
        for(Case objCase : lstCase)
            System.assertEquals('Working', objCase.Status, 'Done');
    }
    
@isTest 
    public static void testBatch() {
        // Create test data
        User user = [SELECT Id,Name From User Limit 1];
        Account testAccount = new Account(Name = 'Demo Account', Email__c = 'demo@example.com');
        insert testAccount;
        Contact testContact = new Contact(FirstName = 'Demo', LastName = 'Contact', Email = 'testcontact@example.com',AccountId = testAccount.Id);
        insert testContact;
        Case testCase = new Case(AccountId = testAccount.Id, OwnerId = user.Id);
        insert testCase;

        // Start the batch
        Test.startTest();
        TaskCaseStatus batch = new TaskCaseStatus();
        Database.executeBatch(batch);
        Test.stopTest();

        // Verify that an email was sent for the test case
        List<Messaging.SingleEmailMessage> sentEmails = new List<Messaging.SingleEmailMessage>();
        System.assertEquals(1, sentEmails.size());
        System.assertEquals(testCase.ContactEmail, sentEmails[0].getToAddresses()[0]);
        System.assertEquals('Case has been Created', sentEmails[0].getSubject());
        System.assertEquals('Case status is modified.', sentEmails[0].getPlainTextBody());
    
}

}