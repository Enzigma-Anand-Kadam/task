/*Need to write batch for all the not closed cases present. 
if they have owner = Queue set status & send email to Account Email AND 
if they have owner = User set status & send email to related contact Email of case */
public with sharing class TaskCaseStatus implements Database.Batchable<sObject>{

    //List<Case> lstEmailCase = [SELECT AccountId, Account.Email__c, ContactEmail FROM Case];

    public Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator('SELECT OwnerId, Owner.Type, ContactEmail, Account.Email__c, Contact.Email FROM Case');
    }

    public void execute(Database.BatchableContext bc, List<Case> lstCase){
        List<Case> lstNewCase = new List<Case>();
        for(Case objCase : lstCase){
            if(objCase.Status != 'Closed' && objCase.Owner.Type == 'Queue'){
                    objCase.Status =  'New';
                    lstNewCase.add(objCase);
            }
                        if(objCase.Owner.Type == 'User'){ 
                            objCase.Status = 'Working';
                            lstNewCase.add(objCase);
                
            }
        }
            update lstNewCase;
        }
    public void finish(Database.BatchableContext bc){

        Account objEmailAccount = new Account();
        Contact objEmailContact = new Contact();
        // Send an email to the manager of the job
        String emailBody = 'Case status is modified.';
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        //mail.setToAddresses(new String[] {objEmailCase.ContactEmail}); // replace with actual email field of the manager
        mail.setToAddresses(new String[] {objEmailAccount.Email__c});
        mail.setToAddresses(new String[] {objEmailContact.Email});
        mail.setSubject('Case has been Created');
        mail.setPlainTextBody(emailBody);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

        /*Map<Id, String> mapEmail = new Map<Id, String>();
    for(Case obj : lstEmailCase){
        mapEmail.put(Obj.AccountId, Obj.AccountId.Email__c);
    }

        for(Case objEmailCase : lstEmailCase){

                // Send an email to the manager of the job
        String emailBody = 'Case status is modified.';
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {objEmailCase.AccountId.Email__c}); // replace with actual email field of the manager
        mail.setSubject('Case has been Created');
        mail.setPlainTextBody(emailBody);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            
        }*/

        

    }
}